import _ from 'lodash';
import './style.scss';
import axios from 'axios';

function component() {
    var element = document.createElement('div');

    // Lodash, currently included via a script, is required for this line to work
    element.innerHTML = _.join(['Hello ', 'webpack'], ' ');
    element.classList.add('hello');


    return element;
}

document.body.appendChild(component());

(() => {
    // strict mode to prevent errors and enable some ES6 features
    'use strict'

    // let's use jQuery's ajax method to keep the code terse.
    // Pull data from randomuser.me as a stub for our 'employee API'
    // (recall that this is really just a fake tweet list).
    axios.get('https://random.dog/woof.json')
        .then(function(response) {
            // console.log(response.data.url);
            var element = document.getElementById('test');
            element.src = response.data.url;
        })
        .catch(function(error) {
            console.log(error);
        });

})()